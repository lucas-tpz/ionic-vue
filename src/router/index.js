import Vue from 'vue'
import { IonicVueRouter } from "@ionic/vue";
import HomePage from '@/views/HomePage.vue';
import AddPage from '@/views/AddPage.vue';
import DetailPage from '@/views/DetailPage.vue';

Vue.use(IonicVueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomePage
  },
  {
    path: '/add',
    name: 'add',
    component: AddPage
  },
  {
    path: '/detail/:id',
    name: 'detail',
    component: DetailPage
  }
]

const router = new IonicVueRouter({
  routes
})

export default router
