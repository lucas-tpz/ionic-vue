import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Ionic from '@ionic/vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@ionic/core/css/core.css'
import '@ionic/core/css/ionic.bundle.css'
import 'animate.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(Ionic)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
