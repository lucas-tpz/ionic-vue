import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todoList: [
      {
        id: 1,
        title: "Sample task 1",
        photos: [
          {
            key: `photo-1`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          },
          {
            key: `photo-2`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          }
        ],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a mi vestibulum, viverra mi a, laoreet diam. Nulla mattis porta."
      },
      {
        id: 2,
        title: "Sample task 2",
        photos: [
          {
            key: `photo-2`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          },
          {
            key: `photo-3`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          }
        ],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a mi vestibulum, viverra mi a, laoreet diam. Nulla mattis porta."
      },
      {
        id: 3,
        title: "Sample task 3",
        photos: [
          {
            key: `photo-3`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          },
          {
            key: `photo-1`,
            url: "https://i.picsum.photos/id/605/200/300.jpg",
            selected: false
          }
        ],
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a mi vestibulum, viverra mi a, laoreet diam. Nulla mattis porta."
      }
    ]
  },
  mutations: {
    addTodo: (state, todo) => {
      state.todoList.push({
        id: state.todoList.length+1,
        title: todo.title,
        photos: todo.photos,
        description: todo.description
      })
    },
    deleteTodo: (state, deleted) => {
      state.todoList = state.todoList.filter(todo => todo !== deleted)
    }
  },
  getters: {
    getTodo: (state) => (id) => state.todoList.find(todo => todo.id === id)
  },
  actions: {
  }
})
